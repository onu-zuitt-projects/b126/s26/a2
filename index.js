//import the contents of the Express package to use for our application
const express = require("express");

//import Mongoose
const mongoose = require("mongoose");

//give the express() function of the express package the variabe "app" so that it can be used
const app = express();

//Mongoose's connect method takes our MongoDB Atlas connection string and uses it to connect to MongoDB Atlas

//useNewUrlParser and useUnifiedTopology are both set to true as part of a newer Mongoose update that allows a better way of connecting to MongoDB Atlas, since the older way is about to be deprecated (become obsolete)

mongoose.connect("mongodb+srv://admin:admin@cluster0.qgo12.mongodb.net/b126_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//confirm that Atlas connection is successful
mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas"))

//Creating a Mongoose Schema

//Schemas determine two things:
//1. They determine the fields in a given resource
//2. They determine the data types of each field

//using the Schema() method of Mongoose lets us define the rules that our data must follow to be validated (allowed)

const taskSchema = new mongoose.Schema({ //the new keyword creates a new schema
	//here we define the fields that MUST be included in each new task as well as what data type they must be
	name: String, //all tasks must have a name, and all names must be a string
	status: {
		type: String, //all tasks must have a status, and all statuses must be a string
		default: "pending" //default values are automatically set
	}
})

//create user schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

// {
// 	"name": "Eat breakfast", <-defined by user
// 	"status": "pending" <-automatically set to pending because of "default"
// }

//After having defined our schema, we can now use it in a Model

//Models use schemas and are used to create the actual data that will be saved or modified in the database

//If the schema is the list of rules, then the Model enforces those rules

const Task = mongoose.model("Task", taskSchema)

//create user model
const User = mongoose.model("User", userSchema)

app.use(express.json()); //allows your server to read and send json data
app.use(express.urlencoded({
	extended: true
})); //allows your server to read data from forms

const port = 4000;

//Express Routing:

//.get below refers to the request method, while "/" refers to the endpoint. When the proper combination of requests is made by a user (in this case, a GET request to our home page), then Express can send our designated response (in this case, a message saying hello from Express)

app.get("/", (req, res) => {
	res.send("Hello Jino from Express!")
})

//route to create new tasks
app.post("/tasks", (req, res) => {
	console.log(req.body)

	Task.findOne({name: req.body.name}, (err, result) => {
		console.log(result)

		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate task found")
		}else{
			// return res.send("No duplicate task found")

			//create a new task object
			let newTask = new Task({
				name: req.body.name,
			})

			//save the new task we created to our database, then check for any errors
			//if any error is found, it is kept inside the "err" parameter
			//in the if statement, if the err is NOT null (meaning there's an error), then we will inform the user that the new task was not created.
			//if there is no error, confirm saving of new task
			newTask.save((err, task) => {
				//if there is no error, err contains null
				//null is a falsy value
				//falsy values in a condition like if() are false
				//so the else statement happens if there is no error
				if(err){
					return res.send("Error creating new task")
				}else{
					return res.send("New task successfully created")
				}
			})
		}
	})
})

//Route to get all tasks
app.get("/tasks", (req, res) => {
	//mongoDB find() without a condition returns ALL documents
	Task.find({}, (err, result) => {
		//if an error is found
		if(err){
			return res.send("Error getting tasks.")
		}else{
			//if no error is found, return results as json
			return res.json({
				data: result
			})
		}
	})
})

//Create a new route for creating a new user when a POST request is sent to the /users endpoint. If either the username or password fields are empty, registration will not take place (return an error message). After that, check for duplicate usernames before creating a new user.

//route to create new users

app.post("/users", (req, res) => {
	console.log(req.body)

	if(req.body.username == "" || req.body.password == "" || (req.body.username == "" && req.body.password == "")){
		return res.send("Error username or password empty.")
	}else{

		User.findOne({username: req.body.username}, (err, result) => {
			console.log(result)

			if(result !== null && result.username === req.body.username){
				return res.send("Duplicate username found")
			}else{
				

				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				})

				newUser.save((err, user) => {
					
					if(err){
						return res.send("Error creating new user")
					}else{
						return res.send("New user successfully created")
					}
				})
			}
		})
	}
})

app.listen(port, () => console.log(`Server running at port ${port}`))
